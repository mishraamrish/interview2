from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

# Create your models here.
class PostLikes(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)

class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    title = models.CharField(max_length=128, null=False, blank=False)
    body = models.CharField(max_length=5000, null=False, blank=False)
    likes = models.ManyToManyField(PostLikes)
