from rest_framework import serializers
from main import models


class PostListSerializer(serializers.ModelSerializer):
    like_count = serializers.SerializerMethodField()
    class Meta:
        model = models.Post
        fields = ("id", "author", "title", "body", "like_count")
    def get_like_count(self, obj):
        # import ipdb; ipdb.set_trace()
        return obj.likes.count()


class PostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Post
        exclude = ("likes",)



class PolstLikeSericalizer(serializers.ModelSerializer):
    class Meta:
        model = models.Post
        fields = ('likes',)
