from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from .serializers import PostListSerializer, PostCreateSerializer, PolstLikeSericalizer
from .models import Post, PostLikes
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

# Create your views here.
class ListPostViewSets(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """
    serializer_class = PostListSerializer
    queryset = Post.objects.all()

class UserPostViewSets(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all()
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        query_set = super().get_queryset()
        if self.request.user.is_superuser:
            return query_set
        else:
            return query_set.filter(author_id=self.request.user.id)

class PostLikeViewSets(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """
    serializer_class = PolstLikeSericalizer
    queryset = Post.objects.all()
#     #
#     # def list(self, request, *args, **kwargs):
#     #     return Response(serializer.data)
#

