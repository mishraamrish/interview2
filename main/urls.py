from django.urls import path
from main import views

urlpatterns = [
    path('list/', views.ListPostViewSets.as_view(
        {
            'get': 'list'
         }
    )),
    path('create/', views.UserPostViewSets.as_view(
        {
            'post': 'create',
         }
    )),
    path('update/<int:pk>/', views.UserPostViewSets.as_view(
        {
            'patch': 'update',
         }
    )),
    path('like/', views.PostLikeViewSets.as_view(
        {
            "post" :'create',
        }
    )),
    path('like/<int:pk>', views.PostLikeViewSets.as_view(
        {
            'patch': 'update',
        }
    )),
]
